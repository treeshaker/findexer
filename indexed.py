
import os
import os.path
import subprocess

#assert sys.version_info[0] >= 3
# Currently this will work in both Python3.0 and Python 2.*.

#./index [ -v -O -n -q ] [ -c <col> ] [ -f <fifoname> ] [ -l <count> ] [ -h <count> ] <file>
#-v verbose log output. (Significantly reduces performance.)
#-O optimize (Implies that requests can be reordered.)
#-n keys are numeric (Otherwise, keys are strings <= 18 chars.)
#-q Don't respond with empty lines for missing keys("quiet mode").
#   Only use this if you are certain you will never pass invalid
#   keys since a process (including the Python wrapper) reading
#   from the fifo may block indefinitely awaiting response.
#-c Index column number <col> (leftmost is #1, not 0.) (default: 1)
#-f use <fifoname> for all IPC
#-l target file contains <= <count> data lines. Else, file is scanned.
#-h skip <count> (presumably header) lines when indexing (default: 0)
#All boolean options default to the opposite state described above.
#The fifo name specified on the command line overrides that in the
#environment variable 'INDEXFIFO'.

class File:
	INDEXER_PATH='index'
	"""
	A friendly interface on the FIFO created by the index program.
	"""
	def __init__( self, filename=None, fifoname=None, numeric=False, column=1, skip=0 ):
		"""
		"""
		if filename is None and fifoname is None:
			raise RuntimeError( 
				"at least one of filename or fifoname must be specified" )
		self.fifoname = fifoname
		self.filename = filename
		self.numeric=numeric
		self.column=column
		self.skip=skip
		self.optimize=False
		# Now build the derivative parts
		self.fifoIsOwned = False
		self.child = None
		# If no fifo name was provided then this class is responsible for 
		# everything: creating the FIFO, exec'ing the indexer, etc...
		if self.fifoname is None:
			self.fifoname = os.path.splitext( self.filename )[0] + '.fifo'
			if not os.path.exists( self.fifoname ):
				os.mkfifo( self.fifoname )
				self.fifoIsOwned = True
		# ...however, only exec the indexer if a filename was provided. 
		# Otherwise, we assume something else has already run the indexer.
		if not filename is None:
			self.child = subprocess.Popen( args = self.__build_cmd() )


	def __del__( self ):
		if self.child:
			fp = open( self.fifoname, 'w' )
			fp.write( '\x04' )
			fp.close()
			os.waitpid( self.child.pid, 0 )
			os.unlink( self.fifoname )


	def __build_cmd( self ):
		"./index [ -v -O -n ] [ -c <col> ] [ -f <fifoname> ] [ -l <count> ] [ -h <count> ] <file>"
		args = [ File.INDEXER_PATH,]
		if self.optimize: args.append('-O')
		if self.numeric:  args.append('-n')
		args.append( '-c'              )
		args.append( str(self.column ) )
		args.append( '-f'              )
		args.append( self.fifoname     )
		args.append( '-h'              )
		args.append( str(self.skip)    )
		args.append( self.filename     )
		return args


	@staticmethod
	def __build_exception( keys ):
		return LookupError( "No results for key(s): "  + str(keys) )


	def fetch( self, keys, prefilt=True ):
		"""
		"""
		# Write the sought identifiers to the fifo...
		fp = open( self.fifoname, 'w' )
		# Have to explicitly check for str since str is also iterable...
		if isinstance( keys, str ):
			#print( keys, file=fp )
			fp.write( keys )
			fp.write( '\n' )
		elif isinstance( keys, int ):
			#print( str(keys), file=fp )
			fp.write( str(keys) )
			fp.write(     '\n'  )
		else:
			try: # assume keys is a (non-string) iterable
				for k in keys:
					#print( k, file=fp )
					fp.write( str(k) )
					fp.write( '\n'   )
			except AttributeError:
				#print( keys, file=fp )
				fp.write( str(k) )
				fp.write( '\n'   )
		fp.close()
		# ...and read back the results.
		fp = open( self.fifoname, 'r' )
		results = fp.readlines()
		fp.close()
		assert isinstance( results, list )

		if len(results) == 0:
			raise File.__build_exception( keys )
		if prefilt:
			results = list( filter( lambda l:len(l) > 1, results ) )
		if len(results) == 0:
			raise File.__build_exception( keys )
		return results

