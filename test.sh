
py3k=/local/bin/python3.0

# Test pre-indexed file w/pre-created FIFO
mkfifo table1.fifo
./index -v -n -c 1 -f table1.fifo -h 1 table1.tab &
$py3k ifile.py table1.fifo 
echo $'\x04' > table1.fifo
./index -v -c 2 -f table1.fifo -h 1 table1.tab &
$py3k ifile.py table1.fifo 
echo $'\x04' > table1.fifo
rm table1.fifo

# Test allowing IndexedFile class to do everything.
$py3k ifile.py table1.tab

